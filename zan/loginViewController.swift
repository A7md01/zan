//
//  loginViewController.swift
//  zan
//
//  Created by ِAhmed bahaa on 9/20/17.
//  Copyright © 2017 ِAhmed bahaa. All rights reserved.
//

import UIKit
import FontAwesome_swift

class loginViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "update profile"
        navigationController?.navigationBar.barTintColor = UIColor(red:118/255.0, green:222/255.0, blue:191/255.0, alpha:1.0)
        
        userName.rightViewMode = UITextFieldViewMode.always
        let name_img = UIImageView(frame: CGRect(x: 0,y: 0,width: 20,height: 20))
        name_img.image = UIImage.fontAwesomeIcon(name: .user
            , textColor: UIColor(red:0.1 ,green: 0.6,blue: 0.3,alpha:0.6), size: CGSize(width:20 , height:20))
        userName.rightView = name_img
        
        userPassword.rightViewMode = UITextFieldViewMode.always
        let pass_img = UIImageView(frame: CGRect(x: 0,y: 0,width: 20,height: 20))
        pass_img.image = UIImage.fontAwesomeIcon(name: .lock
            , textColor: UIColor(red:0.1 ,green: 0.6,blue: 0.3,alpha:0.6), size: CGSize(width:20 , height:20))
        userPassword.rightView = pass_img
        
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let usernameTextField: UITextField = {
        let tf = UITextField()
        
        
        let imageView = UIImageView()
        
        let image = UIImage(named: "user-icon")
        
        imageView.image = image
        imageView.frame = CGRect(x: 0, y: 0, width: 50, height: 15)
        imageView.contentMode = .scaleAspectFit
        tf.leftViewMode = UITextFieldViewMode.always
        tf.leftView = imageView
        tf.addSubview(imageView)
        
        
        var placeholder = NSMutableAttributedString(string: "username", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        tf.attributedPlaceholder = placeholder
        tf.backgroundColor = UIColor.init(red: 35.0/100.0, green: 34.0/100.0, blue: 34.0/100.0, alpha: 1)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.layer.cornerRadius = 25
        tf.textColor = .white
        tf.layer.borderWidth = 1
        tf.layer.borderColor = UIColor(white: 1, alpha: 0.1).cgColor
        tf.layer.masksToBounds = true
        return tf
    }()
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
