//
//  forgetPasswordViewController.swift
//  zan
//
//  Created by ِAhmed bahaa on 9/20/17.
//  Copyright © 2017 ِAhmed bahaa. All rights reserved.
//

import UIKit

class forgetPasswordViewController: UIViewController {

    @IBOutlet weak var userEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        userEmail.rightViewMode = UITextFieldViewMode.always
        let mail_img = UIImageView(frame: CGRect(x: 5,y: 5,width: 20,height: 20))
        mail_img.image = UIImage.fontAwesomeIcon(name: .envelope
            , textColor: UIColor(red:0.1 ,green: 0.6,blue: 0.3,alpha:0.6), size: CGSize(width:20 , height:20))
        userEmail.rightView = mail_img
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
